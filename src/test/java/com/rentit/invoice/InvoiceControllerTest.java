package com.rentit.invoice;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentit.RentitApplication;
import com.rentit.common.application.dto.BusinessPeriodDTO;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.rentit.invoicing.application.dto.InvoiceDto;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.domain.model.POStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentitApplication.class) // Check if the name of this class is correct or not
@WebAppConfiguration
//@Sql("/plants-dataset.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class InvoiceControllerTest {

    @Autowired
    PlantInventoryEntryRepository repo;
    @Autowired
    ObjectMapper mapper;
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testInvoiceSubmission() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2)));
        order.setUserEmail("test9991122@gmail.com");

        result = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.getStatus().name()).isEqualTo(POStatus.OPEN.name());

        mockMvc.perform(post("/api/sales/orders/update/" + purchaseOrderDTO.get_id() + "/PLANT_RETURNED").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        result = mockMvc.perform(get("/api/sales/orders/" + purchaseOrderDTO.get_id()))
                .andExpect(status().isOk())
                .andReturn();

        order = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(order.getStatus().name()).isEqualTo("INVOICED");

        result = mockMvc.perform(get("/api/invoice/" + purchaseOrderDTO.get_id()))
                .andExpect(status().isOk())
                .andReturn();

        InvoiceDto invoiceDto = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<InvoiceDto>() {
        });
        assertThat(invoiceDto.get_id()).isNotNull();
    }

    @Test
    public void testPaymentReminder() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2)));
        order.setUserEmail("test9991122@gmail.com");

        result = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.getStatus().name()).isEqualTo(POStatus.OPEN.name());

        mockMvc.perform(post("/api/sales/orders/update/" + purchaseOrderDTO.get_id() + "/PLANT_RETURNED").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        result = mockMvc.perform(get("/api/sales/orders/" + purchaseOrderDTO.get_id()))
                .andExpect(status().isOk())
                .andReturn();

        order = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(order.getStatus().name()).isEqualTo("INVOICED");

        result = mockMvc.perform(get("/api/invoice/" + purchaseOrderDTO.get_id()))
                .andExpect(status().isOk())
                .andReturn();

        InvoiceDto invoiceDto = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<InvoiceDto>() {
        });
        assertThat(invoiceDto.getAmount().doubleValue()).isEqualTo(purchaseOrderDTO.getTotal().floatValue());
    }

    @Test
    public void testRemitance() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2)));
        order.setUserEmail("test9991122@gmail.com");

        result = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.getStatus().name()).isEqualTo(POStatus.OPEN.name());

        mockMvc.perform(post("/api/sales/orders/update/" + purchaseOrderDTO.get_id() + "/PLANT_RETURNED").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        result = mockMvc.perform(get("/api/sales/orders/" + purchaseOrderDTO.get_id()))
                .andExpect(status().isOk())
                .andReturn();

        order = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(order.getStatus().name()).isEqualTo("INVOICED");

        result = mockMvc.perform(post("/api/invoice/remittance/" + purchaseOrderDTO.get_id()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        result = mockMvc.perform(get("/api/invoice/" + purchaseOrderDTO.get_id()))
                .andExpect(status().isOk())
                .andReturn();

        InvoiceDto invoiceDto = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<InvoiceDto>() {
        });
        assertThat(invoiceDto.isPaid()).isEqualTo(Boolean.TRUE);
    }
}
