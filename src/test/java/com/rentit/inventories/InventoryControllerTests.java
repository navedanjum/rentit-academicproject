package com.rentit.inventories;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentit.RentitApplication;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentitApplication.class) // Check if the name of this class is correct or not
@WebAppConfiguration
//@Sql("/plants-dataset.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class InventoryControllerTests {

    @Autowired
    PlantInventoryEntryRepository repo;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testPlantDetail() throws Exception {
        Long id = Long.valueOf(1);
        MvcResult result = mockMvc.perform(get("/api/inventory/plant/" + id))
                .andExpect(status().isOk())
                .andReturn();

        PlantInventoryEntryDTO plantInventoryEntryDTO = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<PlantInventoryEntryDTO>() {
        });
        assertThat(plantInventoryEntryDTO.get_id()).isEqualTo(id);
        assertThat(plantInventoryEntryDTO.getName()).isEqualTo("Mini excavator");
        assertThat(plantInventoryEntryDTO.getDescription()).isEqualTo("1.5 Tonne Mini excavator");
        assertThat(plantInventoryEntryDTO.getPrice().doubleValue()).isEqualTo(150.0);
    }

    @Test
    public void testPlantDelivery() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/inventory/delivery/2017-03-22"))
                .andExpect(status().isOk())
                .andReturn();
        List<PlantInventoryEntryDTO> plantInventoryEntryDTOS = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });
        assertThat(plantInventoryEntryDTOS.size()).isEqualTo(1);
        assertThat(plantInventoryEntryDTOS.get(0).get_id()).isEqualTo(1L);
    }

    @Test
    public void testCheckingAvailability() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/inventory/check-availability/" + 1 + "/2018-04-14/2018-04-25"))
                .andExpect(status().isOk())
                .andReturn();
        Boolean isAvailable = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Boolean>() {
        });
        assertThat(isAvailable).isEqualTo(Boolean.TRUE);

        result = mockMvc.perform(get("/api/inventory/check-availability/" + 1 + "/2017-03-22/2017-03-24"))
                .andExpect(status().isOk())
                .andReturn();
        isAvailable = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Boolean>() {
        });
        assertThat(isAvailable).isEqualTo(Boolean.FALSE);
    }



}
