package com.rentit.sales.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentit.RentitApplication;
import com.rentit.common.application.dto.BaseResponse;
import com.rentit.common.application.dto.BusinessPeriodDTO;
import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.rentit.inventory.application.dto.PlantReservationDto;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.rentit.sales.application.dto.ExtendOrderRequest;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.domain.model.AcceptOrderRequest;
import com.rentit.sales.domain.model.POStatus;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentitApplication.class) // Check if the name of this class is correct or not
@WebAppConfiguration
//@Sql("/plants-dataset.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SalesRestControllerTests {
    @Autowired
    PlantInventoryEntryRepository repo;
    @Autowired
    ObjectMapper mapper;
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testGetAllPlants() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-03-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        assertThat(plants.size()).isEqualTo(3);

    }


    @Test
    public void testGetPurchaseOrderByIdFound() throws Exception {
        Long id = Long.valueOf(511);
        MvcResult result = mockMvc.perform(get("/api/sales/orders/" + id))
                .andExpect(status().isOk())
                .andReturn();

        PurchaseOrderDTO orders = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(orders.get_id()).isEqualTo(id);
    }

    @Test
    public void testPurchaseOrderStatus() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2)));
        order.setUserEmail("test9991122");

        result = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        result = mockMvc.perform(get("/api/sales/orders/" + purchaseOrderDTO.get_id()))
                .andExpect(status().isOk())
                .andReturn();

        PurchaseOrderDTO orders = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(orders.getStatus().name()).isEqualTo("OPEN");
    }


    @Test
    public void testGetPurchaseOrderByIdNotFound() throws Exception {
        Long id = Long.valueOf(333);
        MvcResult result = mockMvc.perform(get("/api/sales/orders/" + id))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void testCreatePurchaseOrderAcception() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2)));
        order.setUserEmail("test9991122");

        result = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.getStatus().name()).isEqualTo(POStatus.OPEN.name());
    }

    @Test
    public void testCreatePurchaseOrderRejection() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Loader&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(0));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2019, 4, 22), LocalDate.of(2019, 4, 24)));
        order.setUserEmail("test9991122");

        result = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.getStatus().name()).isEqualTo(POStatus.REJECTED.name());
    }

    @Test
    public void testExtension() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2)));
        order.setUserEmail("test9991122");

        result = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.getStatus().name()).isEqualTo(POStatus.OPEN.name());
        ExtendOrderRequest extendOrderRequest = new ExtendOrderRequest();
        extendOrderRequest.setDaysToExtend(2L);
        extendOrderRequest.setPurchaseOrderId(purchaseOrderDTO.get_id());
        mockMvc.perform(post("/api/sales/orders/extend").content(mapper.writeValueAsString(extendOrderRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
        mockMvc.perform(post("/api/sales/orders/update/"+purchaseOrderDTO.get_id()+"/PLANT_DELIVERED").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        mockMvc.perform(post("/api/sales/orders/extend").content(mapper.writeValueAsString(extendOrderRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        result = mockMvc.perform(get("/api/sales/orders/" + purchaseOrderDTO.get_id()))
                .andExpect(status().isOk())
                .andReturn();

        PurchaseOrderDTO newPurchaseOrderDTO = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(newPurchaseOrderDTO.getRentalPeriod().getEndDate()).isEqualTo(purchaseOrderDTO.getRentalPeriod().getEndDate().plusDays(2));

    }

    @Test
    public void testCancelation() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2)));
        order.setUserEmail("test9991122");

        result = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.getStatus().name()).isEqualTo(POStatus.OPEN.name());

        mockMvc.perform(post("/api/sales/orders/update/"+purchaseOrderDTO.get_id()+"/CANCELED_BY_CLIENT").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        result = mockMvc.perform(get("/api/sales/orders/" + purchaseOrderDTO.get_id()))
                .andExpect(status().isOk())
                .andReturn();

        order = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(order.getStatus().name()).isEqualTo("CANCELED_BY_CLIENT");

    }

    @Test
    public void testDispatched() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2)));
        order.setUserEmail("test9991122");

        result = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.getStatus().name()).isEqualTo(POStatus.OPEN.name());

        mockMvc.perform(post("/api/sales/orders/update/"+purchaseOrderDTO.get_id()+"/PLANT_DISPATCHED").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        result = mockMvc.perform(get("/api/sales/orders/" + purchaseOrderDTO.get_id()))
                .andExpect(status().isOk())
                .andReturn();

        order = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(order.getStatus().name()).isEqualTo("PLANT_DISPATCHED");
    }

    @Test
    public void testDelivered() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2)));
        order.setUserEmail("test9991122");

        result = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.getStatus().name()).isEqualTo(POStatus.OPEN.name());

        mockMvc.perform(post("/api/sales/orders/update/"+purchaseOrderDTO.get_id()+"/PLANT_DELIVERED").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        result = mockMvc.perform(get("/api/sales/orders/" + purchaseOrderDTO.get_id()))
                .andExpect(status().isOk())
                .andReturn();

        order = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(order.getStatus().name()).isEqualTo("PLANT_DELIVERED");
    }

    @Test
    public void testRejectedByClient() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2)));
        order.setUserEmail("test9991122");

        result = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.getStatus().name()).isEqualTo(POStatus.OPEN.name());

        mockMvc.perform(post("/api/sales/orders/update/"+purchaseOrderDTO.get_id()+"/PLANT_REJECTED").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        result = mockMvc.perform(get("/api/sales/orders/" + purchaseOrderDTO.get_id()))
                .andExpect(status().isOk())
                .andReturn();

        order = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(order.getStatus().name()).isEqualTo("PLANT_REJECTED");
    }

    @Test
    public void testReturned() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2)));
        order.setUserEmail("test9991122@gmail.com");

        result = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.getStatus().name()).isEqualTo(POStatus.OPEN.name());

        mockMvc.perform(post("/api/sales/orders/update/"+purchaseOrderDTO.get_id()+"/PLANT_RETURNED").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        result = mockMvc.perform(get("/api/sales/orders/" + purchaseOrderDTO.get_id()))
                .andExpect(status().isOk())
                .andReturn();

        order = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(order.getStatus().name()).isEqualTo("INVOICED");
    }

    @Test
    public void testGetAllPendingOrders() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/orders/find-po/PENDING"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> orders = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PurchaseOrderDTO>>() {
        });

        assertThat(orders.size()).isEqualTo(2);
    }

    @Test
    public void validateNewlyCreatedPurchaseOrder() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2)));
        order.setUserEmail("test9991122");

        MvcResult postResult = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(postResult.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.getPlant().get_id()).isEqualTo(plants.get(1).get_id());
        assertThat(purchaseOrderDTO.getRentalPeriod().getStartDate()).isGreaterThanOrEqualTo(LocalDate.now());
        assertThat(purchaseOrderDTO.getRentalPeriod().getStartDate()).isLessThan(purchaseOrderDTO.getRentalPeriod().getEndDate());
        assertThat(purchaseOrderDTO.getRentalPeriod().getEndDate()).isGreaterThan(LocalDate.now());
        assertThat(purchaseOrderDTO.getRentalPeriod().getEndDate()).isGreaterThan(purchaseOrderDTO.getRentalPeriod().getStartDate());

        //Validation rental period starting from past date
        order = new PurchaseOrderDTO();
        order.setPlant(plants.get(2));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now().minusDays(2), LocalDate.now().plusDays(2)));
        mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        //Validation for start_date greater than end_date
        order = new PurchaseOrderDTO();
        order.setPlant(plants.get(2));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now().plusDays(4), LocalDate.now().plusDays(2)));
        mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());


        //Validation for date as null
        order = new PurchaseOrderDTO();
        order.setPlant(plants.get(2));
        order.setRentalPeriod(BusinessPeriodDTO.of(null, null));
        mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void testPurchaseOrderIdentifier() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now().plusDays(2), LocalDate.now().plusDays(4)));
        order.setUserEmail("test9991122");

        MvcResult postResult = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(postResult.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.get_id()).isNotNull();
        String location = postResult.getResponse().getHeader("Location");
        mockMvc.perform(get(location).content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Ignore
    @Test
    public void testPreAllocation() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2017-04-14&endDate=2017-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });
        PurchaseOrderDTO order;
        MvcResult postResult;
        PurchaseOrderDTO purchaseOrderDTO;
        String purchaseOrderLocation = "";
        for (int i = 0; i < 2; i++) {
            order = new PurchaseOrderDTO();
            order.setPlant(plants.get(1));
            order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now().plusDays(2), LocalDate.now().plusDays(4)));
            order.setUserEmail("test9991122");

            postResult = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isCreated()).andReturn();
            purchaseOrderDTO = mapper.readValue(postResult.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
            });
            purchaseOrderLocation = postResult.getResponse().getHeader("Location");
            String poLocation = postResult.getResponse().getHeader("Location");
            mockMvc.perform(post("/api/sales/orders/preallocate/" + purchaseOrderDTO.get_id()).content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isCreated()).andReturn();
            postResult = mockMvc.perform(get(poLocation).content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk()).andReturn();
            purchaseOrderDTO = mapper.readValue(postResult.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
            });
            assertThat(purchaseOrderDTO.getStatus()).isEqualTo(POStatus.PREALLOCATED);
        }

        MvcResult getResult = mockMvc.perform(get("/api/sales/orders/find-po/PREALLOCATED"))
                .andExpect(status().isOk()).andReturn();
        List<PurchaseOrderDTO> purchaseOrderDTOs = mapper.readValue(getResult.getResponse().getContentAsString(), new TypeReference<List<PurchaseOrderDTO>>() {
        });
        assertThat(purchaseOrderDTOs.size()).isEqualTo(2);
        postResult = mockMvc.perform(post("/api/sales//orders/update/" + purchaseOrderDTOs.get(1).get_id() + "/OPEN"))
                .andExpect(status().isOk()).andReturn();
        BaseResponse baseResponse = mapper.readValue(postResult.getResponse().getContentAsString(), new TypeReference<BaseResponse>() {
        });
        assertThat(baseResponse.getMessage()).isEqualTo("Successfully Confirmed");

        order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now().plusDays(2), LocalDate.now().plusDays(4)));
        order.setUserEmail("test9991122");

        postResult = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        purchaseOrderDTO = mapper.readValue(postResult.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        mockMvc.perform(post("/api/sales/orders/preallocate/" + purchaseOrderDTO.get_id()).content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict()).andReturn();
        String poLocation = postResult.getResponse().getHeader("Location");
        postResult = mockMvc.perform(get(poLocation).content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        purchaseOrderDTO = mapper.readValue(postResult.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.getStatus()).isEqualTo(POStatus.REJECTED);

        getResult = mockMvc.perform(get(purchaseOrderLocation))
                .andExpect(status().isOk()).andReturn();
        order = mapper.readValue(getResult.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(order.getStatus()).isEqualTo(POStatus.OPEN);
        assertThat(order.getTotal()).isGreaterThan(new BigDecimal(0));
        BigDecimal days = new BigDecimal(ChronoUnit.DAYS.between(purchaseOrderDTOs.get(1).getRentalPeriod().getStartDate(), purchaseOrderDTOs.get(1).getRentalPeriod().getEndDate()));
        assertThat(order.getTotal()).isEqualTo(plants.get(1).getPrice().multiply(days));

        postResult = mockMvc.perform(post("/api/sales//orders/update/" + purchaseOrderDTOs.get(0).get_id() + "/REJECTED"))
                .andExpect(status().isOk()).andReturn();
        baseResponse = mapper.readValue(postResult.getResponse().getContentAsString(), new TypeReference<BaseResponse>() {
        });
        assertThat(baseResponse.getMessage()).isEqualTo("Successfully Deallocated");
    }

    @Test
    public void testManualAcceptation() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2017-04-14&endDate=2017-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });
        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now().plusDays(2), LocalDate.now().plusDays(4)));
        order.setUserEmail("test9991122");

        MvcResult postResult = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(postResult.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        String purchaseOrderLocation = postResult.getResponse().getHeader("Location");
        String startDate = purchaseOrderDTO.getRentalPeriod().getStartDate().toString();
        String endDate = purchaseOrderDTO.getRentalPeriod().getEndDate().toString();
        result = mockMvc.perform(get("/api/sales/plant-items?name=" + purchaseOrderDTO.getPlant().getName() + "&startDate=" + startDate + "&endDate=" + endDate))
                .andExpect(status().isOk()).andReturn();
        List<PlantInventoryItemDTO> plantInventoryItemDTOS = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryItemDTO>>() {
        });
        assertThat(plantInventoryItemDTOS.size()).isGreaterThan(0);
        AcceptOrderRequest acceptOrderRequest = new AcceptOrderRequest();
        acceptOrderRequest.setItemId(plantInventoryItemDTOS.get(0).get_id());
        acceptOrderRequest.setPurchaseOrderId(purchaseOrderDTO.get_id());
        postResult = mockMvc.perform(post("/api/sales/orders/accept-purchase-order").content(mapper.writeValueAsString(acceptOrderRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        BaseResponse baseResponse = mapper.readValue(postResult.getResponse().getContentAsString(), new TypeReference<BaseResponse>() {
        });
        assertThat(baseResponse.getMessage()).isEqualTo("Purchase Order accepted successfully");

        MvcResult getResult = mockMvc.perform(get(purchaseOrderLocation))
                .andExpect(status().isOk()).andReturn();
        order = mapper.readValue(getResult.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(order.getStatus()).isEqualTo(POStatus.OPEN);
        assertThat(order.getTotal()).isGreaterThan(new BigDecimal(0));
        BigDecimal days = new BigDecimal(ChronoUnit.DAYS.between(purchaseOrderDTO.getRentalPeriod().getStartDate(), purchaseOrderDTO.getRentalPeriod().getEndDate()));
        assertThat(order.getTotal()).isEqualTo(plantInventoryItemDTOS.get(0).getPlantInventoryEntryDTO().getPrice().multiply(days));

        acceptOrderRequest = new AcceptOrderRequest();
        acceptOrderRequest.setItemId(null);
        acceptOrderRequest.setPurchaseOrderId(purchaseOrderDTO.get_id());
        postResult = mockMvc.perform(post("/api/sales/orders/accept-purchase-order").content(mapper.writeValueAsString(acceptOrderRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
        baseResponse = mapper.readValue(postResult.getResponse().getContentAsString(), new TypeReference<BaseResponse>() {
        });
        assertThat(baseResponse.getMessage()).isEqualTo("Item Id cannot be null");

        acceptOrderRequest = new AcceptOrderRequest();
        acceptOrderRequest.setItemId(12345544L);
        acceptOrderRequest.setPurchaseOrderId(purchaseOrderDTO.get_id());
        mockMvc.perform(post("/api/sales/orders/accept-purchase-order").content(mapper.writeValueAsString(acceptOrderRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound()).andReturn();
    }

    @Test
    public void testReservation() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now().plusDays(2), LocalDate.now().plusDays(4)));
        order.setUserEmail("test9991122");

        MvcResult postResult = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(postResult.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTO.get_id()).isNotNull();
        String purchaseOrderLocation = postResult.getResponse().getHeader("Location");
        mockMvc.perform(get(purchaseOrderLocation).content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        String startDate = purchaseOrderDTO.getRentalPeriod().getStartDate().toString();
        String endDate = purchaseOrderDTO.getRentalPeriod().getEndDate().toString();
        result = mockMvc.perform(get("/api/sales/plant-items?name=" + purchaseOrderDTO.getPlant().getName() + "&startDate=" + startDate + "&endDate=" + endDate))
                .andExpect(status().isOk()).andReturn();
        List<PlantInventoryItemDTO> plantInventoryItemDTOS = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<PlantInventoryItemDTO>>() {
        });
        assertThat(plantInventoryItemDTOS.size()).isGreaterThan(0);
        AcceptOrderRequest acceptOrderRequest = new AcceptOrderRequest();
        acceptOrderRequest.setItemId(plantInventoryItemDTOS.get(0).get_id());
        acceptOrderRequest.setPurchaseOrderId(purchaseOrderDTO.get_id());
        postResult = mockMvc.perform(post("/api/sales/orders/accept-purchase-order").content(mapper.writeValueAsString(acceptOrderRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        BaseResponse baseResponse = mapper.readValue(postResult.getResponse().getContentAsString(), new TypeReference<BaseResponse>() {
        });
        assertThat(baseResponse.getMessage()).isEqualTo("Purchase Order accepted successfully");

        MvcResult getResult = mockMvc.perform(get("/api/inventory/reservations?plantId=" +
                plantInventoryItemDTOS.get(0).get_id() + "&startDate=" + startDate + "&endDate=" + endDate))
                .andExpect(status().isOk()).andReturn();
        List<PlantReservationDto> plantReservationDtos = mapper.readValue(getResult.getResponse().getContentAsString(), new TypeReference<List<PlantReservationDto>>() {
        });
        assertThat(plantReservationDtos.size()).isEqualTo(1);
        assertThat(plantReservationDtos.get(0).getPlantInventoryItemDTO().get_id()).isEqualTo(plantInventoryItemDTOS.get(0).get_id());
        assertThat(plantReservationDtos.get(0).getBusinessPeriod()).isEqualTo(BusinessPeriod.of(LocalDate.parse(startDate), LocalDate.parse(endDate)));
    }

}


