insert into plant_inventory_entry (id, name, description, price, delivery_cost, original_price)
    values (1, 'Mini excavator', '1.5 Tonne Mini excavator', 150, 20, 80);
insert into plant_inventory_entry (id, name, description, price, delivery_cost, original_price)
    values (2, 'Mini excavator', '3 Tonne Mini excavator', 200, 30, 90);
insert into plant_inventory_entry (id, name, description, price, delivery_cost, original_price)
    values (3, 'Midi excavator', '5 Tonne Midi excavator', 250, 40, 100);
insert into plant_inventory_entry (id, name, description, price, delivery_cost, original_price)
    values (4, 'Midi excavator', '8 Tonne Midi excavator', 300, 50, 120);
insert into plant_inventory_entry (id, name, description, price, delivery_cost, original_price)
    values (5, 'Maxi excavator', '15 Tonne Large excavator', 400, 60, 150);
insert into plant_inventory_entry (id, name, description, price, delivery_cost, original_price)
    values (6, 'Maxi excavator', '20 Tonne Large excavator', 450, 50, 180);
insert into plant_inventory_entry (id, name, description, price, delivery_cost, original_price)
    values (7, 'HS dumper', '1.5 Tonne Hi-Swivel Dumper', 150, 20, 80);
insert into plant_inventory_entry (id, name, description, price, delivery_cost, original_price)
    values (8, 'FT dumper', '2 Tonne Front Tip Dumper', 180, 30, 100);
insert into plant_inventory_entry (id, name, description, price, delivery_cost, original_price)
    values (9, 'FT dumper', '2 Tonne Front Tip Dumper', 200, 30, 100);
insert into plant_inventory_entry (id, name, description, price, delivery_cost, original_price)
    values (10, 'FT dumper', '2 Tonne Front Tip Dumper', 300, 50, 120);
insert into plant_inventory_entry (id, name, description, price, delivery_cost, original_price)
    values (11, 'FT dumper', '3 Tonne Front Tip Dumper', 400, 60, 150);
insert into plant_inventory_entry (id, name, description, price, delivery_cost, original_price)
    values (12, 'Loader', 'Hewden Backhoe Loader', 200, 50, 90);
insert into plant_inventory_entry (id, name, description, price, delivery_cost, original_price)
    values (13, 'D-Truck', '15 Tonne Articulating Dump Truck', 250, 50, 100);
insert into plant_inventory_entry (id, name, description, price, delivery_cost, original_price)
    values (14, 'D-Truck', '30 Tonne Articulating Dump Truck', 300, 60, 150);

insert into plant_inventory_item (id, plant_info_id, serial_number)
    values (1, 1, 'A01');
insert into plant_inventory_item (id, plant_info_id, serial_number)
    values (2, 2, 'A02');
insert into plant_inventory_item (id, plant_info_id, serial_number)
    values (3, 3, 'A03');

insert into plant_reservation (id, plant_id, start_date, end_date)
    values (1, 1, '2017-03-22', '2017-03-24');

--Creating dummy entry with linked plant reservation for test
insert into purchase_order (id,ISSUE_DATE,PAYMENT_SCHEDULE,START_DATE,END_DATE,STATUS,TOTAL,PLANT_ID)
values (511,'2017-04-15','2017-05-15','2017-04-15','2017-05-15','PENDING',1300,'5');
insert into purchase_order (id,ISSUE_DATE,PAYMENT_SCHEDULE,START_DATE,END_DATE,STATUS,TOTAL,PLANT_ID)
values (611,'2017-04-15','2017-05-15','2017-05-15','2017-05-25','PENDING',1325,'6');