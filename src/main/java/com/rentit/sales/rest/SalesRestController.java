package com.rentit.sales.rest;

import com.rentit.common.application.dto.BaseResponse;
import com.rentit.common.application.exceptions.NoPlantAvailableException;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.rentit.inventory.application.services.InventoryService;
import com.rentit.sales.application.dto.ExtendOrderRequest;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.application.services.SalesService;
import com.rentit.sales.domain.model.AcceptOrderRequest;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.rest.validator.AcceptOrderValidator;
import com.rentit.sales.rest.validator.PurchaseOrderDtoValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/sales")
@Slf4j
public class SalesRestController {

    @Autowired
    InventoryService inventoryService;

    @Autowired
    SalesService salesService;

    @Autowired
    AcceptOrderValidator acceptOrderValidator;

    @Autowired
    PurchaseOrderDtoValidator purchaseOrderDtoValidator;

    @InitBinder("acceptOrderRequest")
    protected void initBinderForAcceptOrder(WebDataBinder binder) {
        binder.addValidators(acceptOrderValidator);
    }

    @InitBinder("purchaseOrderDTO")
    protected void initBinderForPurchaseOrderDTO(WebDataBinder binder) {
        binder.addValidators(purchaseOrderDtoValidator);
    }

    @GetMapping("/plants")
    public List<PlantInventoryEntryDTO> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return inventoryService.findAvailable(plantName, startDate, endDate);
    }

    @GetMapping("/plant-items")
    public List<PlantInventoryItemDTO> findAvailablePlantItems(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return inventoryService.findAvailablePlantItem(plantName, startDate, endDate);
    }

    @GetMapping("/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PurchaseOrderDTO fetchPurchaseOrder(@PathVariable("id") Long id) {
        return salesService.findPurchaseOrder(id);
    }

    @PostMapping("/orders/accept-purchase-order")
    @ResponseStatus(HttpStatus.OK)
    public BaseResponse acceptPurchaseOrder(@RequestBody @Validated AcceptOrderRequest acceptOrderRequest) {
        log.warn("accept Order request: {}", acceptOrderRequest);
        salesService.acceptOrder(acceptOrderRequest.getPurchaseOrderId(), acceptOrderRequest.getItemId());
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setMessage("Purchase Order accepted successfully");
        return baseResponse;
    }

    @PostMapping("/orders")
    public ResponseEntity<PurchaseOrderDTO> createPurchaseOrder(@RequestBody @Validated PurchaseOrderDTO purchaseOrderDTO) throws URISyntaxException {
        log.warn("purchase order: {}", purchaseOrderDTO);
        PurchaseOrderDTO newlyCreatePODTO = salesService.createPurchaseOrder(purchaseOrderDTO);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create(newlyCreatePODTO.getId().getHref()));

        log.warn("created purchase order: {}", newlyCreatePODTO);
        return new ResponseEntity<>(newlyCreatePODTO, headers, HttpStatus.CREATED);
    }

    @PostMapping("/orders/extend")
    public BaseResponse extendPurchaseOrder (@RequestBody ExtendOrderRequest extendOrderRequest) {
        log.warn("Entension request: {}", extendOrderRequest);
        salesService.extendPurchaseOrder(extendOrderRequest);
        return new BaseResponse("Purchase Order extended Successfully");
    }

    @GetMapping("/orders/find-po/{status}")
    @ResponseStatus(HttpStatus.OK)
    public List<PurchaseOrderDTO> findPurchaseOrder(@PathVariable POStatus status) {
        return salesService.findPurchaseOrder(status);
    }

    @PostMapping("/orders/preallocate/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public void preAllocatePO(@PathVariable Long id) {
        PurchaseOrderDTO purchaseOrderDTO = salesService.preallocatePO(id);
        if (purchaseOrderDTO.getStatus() == POStatus.REJECTED) {
            throw new NoPlantAvailableException();
        }
    }

    @PostMapping("/orders/update/{id}/{status}")
    @ResponseStatus(HttpStatus.OK)
    public BaseResponse updatePreAllocation(@PathVariable Long id, @PathVariable POStatus status) {
        PurchaseOrderDTO purchaseOrderDTO = salesService.updatePreallocation(id, status);
        BaseResponse baseResponse = new BaseResponse();
        if (purchaseOrderDTO.getStatus() == POStatus.REJECTED) {
            baseResponse.setMessage("Successfully Deallocated");
        } else {
            baseResponse.setMessage("Successfully Confirmed");
        }
        return baseResponse;
    }
}
