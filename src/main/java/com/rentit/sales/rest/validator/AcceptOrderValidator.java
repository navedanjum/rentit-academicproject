package com.rentit.sales.rest.validator;

import com.rentit.sales.domain.model.AcceptOrderRequest;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class AcceptOrderValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return AcceptOrderRequest.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        AcceptOrderRequest acceptOrderRequest = (AcceptOrderRequest) o;

        if(acceptOrderRequest.getItemId() == null) {
            errors.rejectValue("itemId", "Item Id cannot be null");
        }
        if(acceptOrderRequest.getPurchaseOrderId() == null) {
            errors.rejectValue("purchaseOrderId", "purchaseOrderId cannot be null");
        }
    }
}
