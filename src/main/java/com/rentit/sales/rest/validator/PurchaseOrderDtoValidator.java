package com.rentit.sales.rest.validator;

import com.rentit.sales.application.dto.PurchaseOrderDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.time.LocalDate;

@Slf4j
@Component
public class PurchaseOrderDtoValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return PurchaseOrderDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PurchaseOrderDTO purchaseOrderDTO = (PurchaseOrderDTO) o;

        log.warn("purchase Order: {}", purchaseOrderDTO);

        if (purchaseOrderDTO.getPlant() == null) {
            errors.rejectValue("plant", "plan cannot be null");
        }

        if (purchaseOrderDTO.getUserEmail() == null) {
            errors.rejectValue("userEmail", "userEmail cannot be null");
        }

        if (purchaseOrderDTO.getPlant() != null && purchaseOrderDTO.getPlant().get_id() == null) {
            errors.rejectValue("plant._id", "plant must have an Id");
        }

        if (purchaseOrderDTO.getRentalPeriod() == null) {
            errors.rejectValue("rentalPeriod", "rentalPeriod must be given");
        }

        if (purchaseOrderDTO.getRentalPeriod() != null && purchaseOrderDTO.getRentalPeriod().getStartDate() == null) {
            errors.rejectValue("rentalPeriod.startDate", "startDate must be given");
        }

        if (purchaseOrderDTO.getRentalPeriod() != null && purchaseOrderDTO.getRentalPeriod().getEndDate() == null) {
            errors.rejectValue("rentalPeriod.endDate", "endDate must be given");
        }

        if(purchaseOrderDTO.getRentalPeriod() != null && purchaseOrderDTO.getRentalPeriod().getEndDate() != null
                && purchaseOrderDTO.getRentalPeriod().getEndDate().isBefore(LocalDate.now())) {
            errors.rejectValue("rentalPeriod.endDate", "endDate must be in future");
        }

        if(purchaseOrderDTO.getRentalPeriod() != null && purchaseOrderDTO.getRentalPeriod().getStartDate() != null
                && purchaseOrderDTO.getRentalPeriod().getStartDate().isBefore(LocalDate.now())) {
            errors.rejectValue("rentalPeriod.startDate", "startDate must be in future");
        }


        if(purchaseOrderDTO.getRentalPeriod() != null && purchaseOrderDTO.getRentalPeriod().getStartDate() != null &&
                purchaseOrderDTO.getRentalPeriod().getEndDate() != null &&
                purchaseOrderDTO.getRentalPeriod().getEndDate().isBefore(purchaseOrderDTO.getRentalPeriod().getStartDate())) {
            errors.rejectValue("rentalPeriod.endDate", "endDate must be after startDate");
        }
    }
}
