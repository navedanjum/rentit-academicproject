package com.rentit.sales.domain.model;

public enum PreAllocationStatus {
    CONFIRM, REJECT
}
