package com.rentit.sales.domain.model;

import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantReservation;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class PurchaseOrder {
    @Id
    @GeneratedValue
    Long id;

    @OneToMany
    List<PlantReservation> reservations = new ArrayList<>();

    @Setter
    @ManyToOne
    PlantInventoryEntry plant;

    LocalDate issueDate;

    LocalDate paymentSchedule;

    @Setter
    @Column(precision = 8, scale = 2)
    BigDecimal total;

    @Setter
    @Enumerated(EnumType.STRING)
    POStatus status;

    @Embedded
    @Setter
    BusinessPeriod rentalPeriod;

    String userEmail;

    public static PurchaseOrder of(PlantInventoryEntry plant, BusinessPeriod rentalPeriod, String userEmail) {
        PurchaseOrder po = new PurchaseOrder();
        po.plant = plant;
        po.rentalPeriod = rentalPeriod;
        po.status = POStatus.PENDING;
        po.userEmail = userEmail;
        return po;
    }
}
