package com.rentit.sales.domain.model;

import lombok.Data;

@Data
public class AcceptOrderRequest {

    private Long purchaseOrderId;
    private Long itemId;
}
