package com.rentit.sales.application.dto;

import lombok.Data;

@Data
public class ExtendOrderRequest {

    private Long purchaseOrderId;
    private Long daysToExtend;
}
