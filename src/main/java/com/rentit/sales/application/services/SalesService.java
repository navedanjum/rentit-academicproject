package com.rentit.sales.application.services;

import com.rentit.common.application.dto.BusinessPeriodDTO;
import com.rentit.common.application.exceptions.PlantInventoryItemNotFoundException;
import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.application.exceptions.PurchaseOrderNotFoundException;
import com.rentit.common.application.exceptions.StatusChangeException;
import com.rentit.common.application.service.NotificationService;
import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.rentit.inventory.application.services.InventoryService;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import com.rentit.inventory.domain.repository.PlantReservationRepository;
import com.rentit.invoicing.application.service.InvoiceService;
import com.rentit.sales.application.dto.ExtendOrderRequest;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class SalesService {

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    @Autowired
    InventoryService inventoryService;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    NotificationService notificationService;

    @Autowired
    InvoiceService invoiceService;

    public PurchaseOrderDTO findPurchaseOrder(Long id) {
        PurchaseOrder po = purchaseOrderRepository.findOne(id);
        if (po == null) {
            throw new PurchaseOrderNotFoundException(id);
        }
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO createPurchaseOrder(PurchaseOrderDTO partialPODTO) {

        PlantInventoryEntry plant = plantInventoryEntryRepository.findOne(partialPODTO.getPlant().get_id());
        if (plant == null) {
            throw new PlantNotFoundException(partialPODTO.getPlant().get_id());
        }

        BusinessPeriod rentalPeriod = BusinessPeriod.of(partialPODTO.getRentalPeriod().getStartDate(), partialPODTO.getRentalPeriod().getEndDate());

        PurchaseOrder purchaseOrder = PurchaseOrder.of(
                plant,
                rentalPeriod,
                partialPODTO.getUserEmail());
        purchaseOrder = purchaseOrderRepository.save(purchaseOrder);

        List<PlantInventoryItemDTO> plantInventoryItemDTOS = inventoryService
                .findAvailablePlantItem(purchaseOrder.getPlant().getName(), purchaseOrder.getRentalPeriod().getStartDate(),
                        purchaseOrder.getRentalPeriod().getEndDate());
        if (plantInventoryItemDTOS.isEmpty()) {
            purchaseOrder.setStatus(POStatus.REJECTED);
            purchaseOrder = purchaseOrderRepository.saveAndFlush(purchaseOrder);
        } else {
            inventoryService.createReservation(plantInventoryItemRepository
                    .findOne(plantInventoryItemDTOS.get(0).get_id()), purchaseOrder);
            purchaseOrder.setStatus(POStatus.OPEN);
            BigDecimal days = new BigDecimal(ChronoUnit.DAYS.between(purchaseOrder.getRentalPeriod().getStartDate(), purchaseOrder.getRentalPeriod().getEndDate()));
            purchaseOrder.setTotal(purchaseOrder.getPlant().getPrice().multiply(days));
            purchaseOrder = purchaseOrderRepository.saveAndFlush(purchaseOrder);
        }
        return purchaseOrderAssembler.toResource(purchaseOrder);
    }

    public void extendPurchaseOrder(ExtendOrderRequest extendOrderRequest) {

        PurchaseOrder purchaseOrder = purchaseOrderRepository.getOne(extendOrderRequest.getPurchaseOrderId());

        if(purchaseOrder == null) {
            throw new PurchaseOrderNotFoundException(extendOrderRequest.getPurchaseOrderId());
        }

        if(purchaseOrder.getStatus() != POStatus.PLANT_DELIVERED) {
            throw new StatusChangeException("Purchase order not in Delivered status");
        }

        if(inventoryService.checkAvailability(purchaseOrder.getPlant().getId(),
                purchaseOrder.getRentalPeriod().getEndDate(),
                purchaseOrder.getRentalPeriod().getEndDate().plusDays(extendOrderRequest.getDaysToExtend()))) {
            purchaseOrder.setRentalPeriod(BusinessPeriod.of(purchaseOrder.getRentalPeriod().getStartDate(),
                    purchaseOrder.getRentalPeriod().getEndDate().plusDays(extendOrderRequest.getDaysToExtend())));
            purchaseOrder.setTotal(purchaseOrder.getTotal().add(purchaseOrder.getPlant().getPrice().multiply(new BigDecimal(extendOrderRequest.getDaysToExtend()))));
            purchaseOrderRepository.saveAndFlush(purchaseOrder);
            notificationService.notifyAboutExtension(true, purchaseOrder, extendOrderRequest.getDaysToExtend());
        } else {
            List<PlantInventoryEntryDTO> plantInventoryEntryDTOS = inventoryService.findAvailable(purchaseOrder.getPlant().getName(), purchaseOrder.getRentalPeriod().getEndDate(),
                    purchaseOrder.getRentalPeriod().getEndDate().plusDays(extendOrderRequest.getDaysToExtend()));
            if(plantInventoryEntryDTOS.size() == 0) {
                notificationService.notifyAboutExtension(false, purchaseOrder, extendOrderRequest.getDaysToExtend());
            }
            PlantInventoryEntryDTO newPurchaseOrderDto = null;
            for (PlantInventoryEntryDTO plantInventoryEntryDTO : plantInventoryEntryDTOS) {
                BigDecimal income = new BigDecimal(extendOrderRequest.getDaysToExtend())
                        .multiply(plantInventoryEntryDTO.getPrice().subtract(plantInventoryEntryDTO.getOriginalPrice()));
                if(income.multiply(new BigDecimal(0.3)).floatValue() > plantInventoryEntryDTO.getDeliveryCost().floatValue()){
                    newPurchaseOrderDto = plantInventoryEntryDTO;
                    break;
                }
            }
            if(newPurchaseOrderDto == null) {
                notificationService.notifyAboutExtension(false, purchaseOrder, extendOrderRequest.getDaysToExtend());
                return;
            }
            PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO();
            purchaseOrderDTO.setUserEmail(purchaseOrder.getUserEmail());
            purchaseOrderDTO.setRentalPeriod(BusinessPeriodDTO.of(purchaseOrder.getRentalPeriod().getEndDate(),
                    purchaseOrder.getRentalPeriod().getEndDate().plusDays(extendOrderRequest.getDaysToExtend())));
            purchaseOrderDTO.setPlant(newPurchaseOrderDto);
            createPurchaseOrder(purchaseOrderDTO);
        }
    }

    public List<PurchaseOrderDTO> findPurchaseOrder(POStatus poStatus) {
        List<PurchaseOrder> purchaseOrders = purchaseOrderRepository.findByStatus(poStatus);
        return purchaseOrderAssembler.toResources(purchaseOrders);
    }

    public void acceptOrder(Long purchaseOrderId, Long itemId) {
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findOne(purchaseOrderId);
        if (purchaseOrder == null) {
            throw new PurchaseOrderNotFoundException(purchaseOrderId);
        }
        PlantInventoryItem plantInventoryItem = plantInventoryItemRepository.findOne(itemId);
        if (plantInventoryItem == null) {
            throw new PlantInventoryItemNotFoundException(itemId);
        }
        purchaseOrder.setStatus(POStatus.OPEN);
        purchaseOrder.setPlant(plantInventoryItem.getPlantInfo());
        BigDecimal days = new BigDecimal(ChronoUnit.DAYS.between(purchaseOrder.getRentalPeriod().getStartDate(), purchaseOrder.getRentalPeriod().getEndDate()));
        purchaseOrder.setTotal(plantInventoryItem.getPlantInfo().getPrice().multiply(days));
        inventoryService.createReservation(plantInventoryItem, purchaseOrder);
        purchaseOrderRepository.saveAndFlush(purchaseOrder);
    }

    @Transactional
    public PurchaseOrderDTO preallocatePO(Long id) {
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findOne(id);
        if (purchaseOrder == null) {
            throw new PurchaseOrderNotFoundException(id);
        }
        List<PlantInventoryItemDTO> plantInventoryItemDTOS = inventoryService
                .findAvailablePlantItem(purchaseOrder.getPlant().getName(), purchaseOrder.getRentalPeriod().getStartDate(),
                        purchaseOrder.getRentalPeriod().getEndDate());
        if (plantInventoryItemDTOS.isEmpty()) {
            purchaseOrder.setStatus(POStatus.REJECTED);
            purchaseOrder = purchaseOrderRepository.saveAndFlush(purchaseOrder);
        } else {
            inventoryService.createReservation(plantInventoryItemRepository
                    .findOne(plantInventoryItemDTOS.get(0).get_id()), purchaseOrder);
            purchaseOrder.setStatus(POStatus.PREALLOCATED);
            purchaseOrder = purchaseOrderRepository.saveAndFlush(purchaseOrder);
        }
        return purchaseOrderAssembler.toResource(purchaseOrder);
    }

    @Transactional
    public PurchaseOrderDTO updatePreallocation(Long id, POStatus status) {
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findOne(id);
        if (purchaseOrder == null) {
            throw new PurchaseOrderNotFoundException(id);
        }

        switch (status){
            case OPEN: {
                BigDecimal days = new BigDecimal(ChronoUnit.DAYS.between(purchaseOrder.getRentalPeriod().getStartDate(), purchaseOrder.getRentalPeriod().getEndDate()));
                purchaseOrder.setTotal(purchaseOrder.getPlant().getPrice().multiply(days));
                break;
            }
            case REJECTED: {
                if (purchaseOrder.getStatus() == POStatus.OPEN
                        || purchaseOrder.getStatus() == POStatus.PREALLOCATED) {
                    List<PlantReservation> plantReservations = inventoryService.getPlantReservationByPurchaseOrder(purchaseOrder.getId());
                    inventoryService.deleteReservation(plantReservations);
                }
                break;
            }
            case CANCELED_BY_CLIENT: {
                if (purchaseOrder.getStatus() == POStatus.PLANT_DISPATCHED
                        || purchaseOrder.getStatus() == POStatus.PLANT_DELIVERED
                        || purchaseOrder.getStatus() == POStatus.PLANT_RETURNED
                        || purchaseOrder.getStatus() == POStatus.INVOICED
                        || purchaseOrder.getStatus() == POStatus.PLANT_REJECTED) {
                    throw new StatusChangeException("Can't change status form " + purchaseOrder.getStatus().name() + " to " + status.name());
                }
                List<PlantReservation> plantReservations = inventoryService.getPlantReservationByPurchaseOrder(purchaseOrder.getId());
                inventoryService.deleteReservation(plantReservations);
                break;
            }
            case PLANT_REJECTED: {
                List<PlantReservation> plantReservations = inventoryService.getPlantReservationByPurchaseOrder(purchaseOrder.getId());
                inventoryService.deleteReservation(plantReservations);
                break;
            }
            case PLANT_RETURNED: {
                invoiceService.createInvoice(purchaseOrder);
                status = POStatus.INVOICED;
                break;
            }
        }
        purchaseOrder.setStatus(status);
        return purchaseOrderAssembler.toResource(purchaseOrderRepository.saveAndFlush(purchaseOrder));
    }

}
