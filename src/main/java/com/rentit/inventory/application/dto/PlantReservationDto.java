package com.rentit.inventory.application.dto;

import com.rentit.common.domain.model.BusinessPeriod;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

@Data
public class PlantReservationDto extends ResourceSupport {

    private PlantInventoryItemDTO plantInventoryItemDTO;

    private BusinessPeriod businessPeriod;

}
