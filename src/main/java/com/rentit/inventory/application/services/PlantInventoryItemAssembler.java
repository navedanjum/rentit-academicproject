package com.rentit.inventory.application.services;

import com.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantInventoryItemAssembler extends ResourceAssemblerSupport<PlantInventoryItem, PlantInventoryItemDTO> {

    @Autowired
    private PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    public PlantInventoryItemAssembler() {
        super(PlantInventoryItem.class, PlantInventoryItemDTO.class);
    }

    @Override
    public PlantInventoryItemDTO toResource(PlantInventoryItem entity) {
        PlantInventoryItemDTO plantInventoryItemDTO = new PlantInventoryItemDTO();
        plantInventoryItemDTO.set_id(entity.getId());
        plantInventoryItemDTO.setPlantInventoryEntryDTO(plantInventoryEntryAssembler.toResource(entity.getPlantInfo()));
        return plantInventoryItemDTO;
    }
}
