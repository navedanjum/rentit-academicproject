package com.rentit.inventory.application.services;

import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.rentit.inventory.application.dto.PlantReservationDto;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.inventory.domain.repository.InventoryRepository;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import com.rentit.inventory.domain.repository.PlantReservationRepository;
import com.rentit.sales.domain.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class InventoryService {

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;

    @Autowired
    PlantReservationAssembler plantReservationAssembler;

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    public List<PlantInventoryEntryDTO> findAvailable(String plantName, LocalDate startDate, LocalDate endDate) {
        List<PlantInventoryEntry> res = inventoryRepository.findAvailablePlants(plantName, startDate, endDate);
        return plantInventoryEntryAssembler.toResources(res);
    }

    public PlantReservation createReservation(PlantInventoryItem plantInventoryItem, PurchaseOrder purchaseOrder) {
        PlantReservation plantReservation = new PlantReservation();
        plantReservation.setPlant(plantInventoryItem);
        plantReservation.setSchedule(purchaseOrder.getRentalPeriod());
        plantReservation.setPurchaseOrder(purchaseOrder);
        return plantReservationRepository.saveAndFlush(plantReservation);
    }

    public List<PlantInventoryItemDTO> findAvailablePlantItem(String name, LocalDate startDate, LocalDate endDate) {
        List<PlantInventoryItem> plantInventoryItems = inventoryRepository.findAvailablePlantItems(name, startDate, endDate);
        return plantInventoryItemAssembler.toResources(plantInventoryItems);
    }

    public List<PlantReservationDto> getPlantReservations(Long itemId, LocalDate startDate, LocalDate endDate) {
        return plantReservationAssembler.toResources(plantReservationRepository.findByPlant_IdAndSchedule_StartDateAndSchedule_EndDate(itemId, startDate, endDate));
    }

    public void deleteReservation(List<PlantReservation> plantReservations) {
        plantReservationRepository.delete(plantReservations);
    }

    public List<PlantReservation> getPlantReservationByPurchaseOrder(long purchasaeOrderId) {
        return plantReservationRepository.findByPurchaseOrder_Id(purchasaeOrderId);
    }

    public PlantInventoryEntryDTO getPlant(Long id) {
        PlantInventoryEntry plantInventoryEntry = plantInventoryEntryRepository.findOne(id);
        return plantInventoryEntryAssembler.toResource(plantInventoryEntry);
    }

    public Boolean checkAvailability(Long plantId, LocalDate startDate, LocalDate endDate) {
        return inventoryRepository.checkAvailability(plantId, startDate, endDate);
    }

    public List<PlantInventoryEntryDTO> getPlantsToDeliver(LocalDate startDate) {
        return plantInventoryEntryAssembler.toResources(inventoryRepository.findPlantsToDeliver(startDate));
    }
}
