package com.rentit.inventory.application.services;

import com.rentit.inventory.application.dto.PlantReservationDto;
import com.rentit.inventory.domain.model.PlantReservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantReservationAssembler extends ResourceAssemblerSupport<PlantReservation, PlantReservationDto> {

    @Autowired
    private PlantInventoryItemAssembler plantInventoryItemAssembler;

    public PlantReservationAssembler() {
        super(PlantReservation.class, PlantReservationDto.class);
    }

    @Override
    public PlantReservationDto toResource(PlantReservation entity) {
        PlantReservationDto plantReservationDto = new PlantReservationDto();
        plantReservationDto.setBusinessPeriod(entity.getSchedule());
        plantReservationDto.setPlantInventoryItemDTO(plantInventoryItemAssembler.toResource(entity.getPlant()));
        return plantReservationDto;
    }
}
