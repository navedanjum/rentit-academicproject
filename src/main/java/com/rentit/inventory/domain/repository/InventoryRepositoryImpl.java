package com.rentit.inventory.domain.repository;

import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;

public class InventoryRepositoryImpl implements CustomInventoryRepository {
    @Autowired
    EntityManager em;

    @Override
    public List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        return em.createQuery("select i.plantInfo from PlantInventoryItem i where lower(i.plantInfo.name) like concat('%', ?1, '%') and i not in " +
                        "(select r.plant from PlantReservation r where ?2 < r.schedule.endDate and ?3 > r.schedule.startDate)"
                , PlantInventoryEntry.class)
                .setParameter(1, name.toLowerCase())
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getResultList();
    }

    @Override
    public List<PlantInventoryItem> findAvailablePlantItems(String name, LocalDate startDate, LocalDate endDate) {
        return em.createQuery("select i from PlantInventoryItem i where lower(i.plantInfo.name) like concat('%', ?1, '%') and i not in " +
                        "(select r.plant from PlantReservation r where ?2 < r.schedule.endDate and ?3 > r.schedule.startDate)"
                , PlantInventoryItem.class)
                .setParameter(1, name.toLowerCase())
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getResultList();
    }

    @Override
    public Boolean checkAvailability(Long plantId, LocalDate startDate, LocalDate endDate) {
        List<PlantInventoryEntry> plantInventoryEntries = em.createQuery("select i.plantInfo from PlantInventoryItem i where i.plantInfo.id = ?1 and i not in " +
                        "(select r.plant from PlantReservation r where ?2 < r.schedule.endDate and ?3 > r.schedule.startDate)"
                , PlantInventoryEntry.class)
                .setParameter(1, plantId)
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getResultList();
        return plantInventoryEntries.size() > 0;
    }

    @Override
    public List<PlantInventoryEntry> findPlantsToDeliver(LocalDate startDate) {
        return em.createQuery("select i.plantInfo from PlantInventoryItem i where i in " +
                        "(select r.plant from PlantReservation r where ?1 = r.schedule.startDate)"
                , PlantInventoryEntry.class)
                .setParameter(1, startDate)
                .getResultList();
    }
}
