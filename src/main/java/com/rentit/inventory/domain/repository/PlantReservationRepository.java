package com.rentit.inventory.domain.repository;

import com.rentit.inventory.domain.model.PlantReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PlantReservationRepository extends JpaRepository<PlantReservation, Long> {

    List<PlantReservation> findByPlant_IdAndSchedule_StartDateAndSchedule_EndDate(Long id, LocalDate startDate, LocalDate endDate);

    List<PlantReservation> findByPurchaseOrder_Id(Long id);
}
