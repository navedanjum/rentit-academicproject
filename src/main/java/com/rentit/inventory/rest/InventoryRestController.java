package com.rentit.inventory.rest;

import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.dto.PlantReservationDto;
import com.rentit.inventory.application.services.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/inventory")
public class InventoryRestController {

    @Autowired
    private InventoryService inventoryService;

    @GetMapping("/reservations")
    public List<PlantReservationDto> findReservation(@RequestParam(name = "plantId") Long id,
                                                     @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                                     @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return inventoryService.getPlantReservations(id, startDate, endDate);
    }

    @GetMapping("/plant/{plantId}")
    public PlantInventoryEntryDTO getPlantDetails(@PathVariable Long plantId) {
        return inventoryService.getPlant(plantId);
    }

    @GetMapping("/check-availability/{plantId}/{startDate}/{endDate}")
    public Boolean checkAvailability(@PathVariable Long plantId,
                                     @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                     @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return inventoryService.checkAvailability(plantId, startDate, endDate);
    }

    @GetMapping("/delivery/{startDate}")
    public List<PlantInventoryEntryDTO> getPlantsToDeliver(@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate) {
        return inventoryService.getPlantsToDeliver(startDate);
    }
}
