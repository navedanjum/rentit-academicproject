package com.rentit.invoicing.rest;

import com.rentit.invoicing.application.dto.InvoiceDto;
import com.rentit.invoicing.application.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/invoice/")
public class InvoiceRestController {

    @Autowired
    private InvoiceService invoiceService;

    @PostMapping("/remittance/{purchaseOrderId}")
    @ResponseStatus(HttpStatus.OK)
    public void updateRemittance(@PathVariable Long purchaseOrderId) {
        invoiceService.remittence(purchaseOrderId);
    }


    @GetMapping("/{id}")
    public InvoiceDto getInvoice(@PathVariable Long id) {
        return invoiceService.getInvoice(id);
    }

    @GetMapping("/all/{email}")
    public List<InvoiceDto> getAll(@PathVariable String email) {
        return invoiceService.getAllInvoices(email);
    }
}
