package com.rentit.invoicing.application.service;

import com.rentit.invoicing.application.dto.InvoiceDto;
import com.rentit.sales.domain.model.PurchaseOrder;

import java.util.List;

public interface InvoiceService {

    InvoiceDto createInvoice(PurchaseOrder purchaseOrder);

    void sendReminder();

    InvoiceDto remittence(Long purchaseOrderId);

    InvoiceDto getInvoice(Long purchaseOrderId);

    List<InvoiceDto> getAllInvoices(String email);
}
