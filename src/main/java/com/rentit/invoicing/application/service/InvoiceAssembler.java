package com.rentit.invoicing.application.service;

import com.rentit.invoicing.application.dto.InvoiceDto;
import com.rentit.invoicing.domain.model.Invoice;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class InvoiceAssembler extends ResourceAssemblerSupport<Invoice, InvoiceDto> {

    public InvoiceAssembler() {
        super(Invoice.class, InvoiceDto.class);
    }

    @Override
    public InvoiceDto toResource(Invoice entity) {
        InvoiceDto invoiceDto = new InvoiceDto();
        invoiceDto.set_id(entity.getId());
        invoiceDto.setAmount(entity.getAmount());
        invoiceDto.setDueDate(entity.getDueDate());
        invoiceDto.setPaid(entity.isPaid());
        invoiceDto.setPaidDate(entity.getPaidDate());
        invoiceDto.setPurchaseOrderId(entity.getPurchaseOrder().getId());
        return invoiceDto;
    }
}
