package com.rentit.invoicing.application.service;

import com.rentit.common.application.exceptions.InvoiceNotFoundException;
import com.rentit.common.application.service.NotificationService;
import com.rentit.invoicing.application.dto.InvoiceDto;
import com.rentit.invoicing.domain.model.Invoice;
import com.rentit.invoicing.domain.repositories.InvoiceRepository;
import com.rentit.sales.domain.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    private InvoiceAssembler invoiceAssembler;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Override
    public InvoiceDto createInvoice(PurchaseOrder purchaseOrder) {
        Invoice invoice = new Invoice();
        invoice.setAmount(purchaseOrder.getTotal());
        invoice.setDueDate(purchaseOrder.getRentalPeriod().getEndDate().plusDays(15));
        invoice.setPaid(Boolean.FALSE);
        invoice.setPurchaseOrder(purchaseOrder);
        invoice.setPaidDate(null);
        invoice = invoiceRepository.save(invoice);
        notificationService.invoiceCreation(invoice);
        return invoiceAssembler.toResource(invoice);
    }

    @Scheduled(cron = "0 0 4 * * FRI")
    @Override
    public void sendReminder() {

        List<Invoice> invoices = invoiceRepository.findByPaid(false);

        for (Invoice i : invoices) {
            notificationService.invoiceReminder(i);
        }
    }

    @Override
    public InvoiceDto remittence(Long purchaseOrderId) {
        Invoice invoice = invoiceRepository.findByPurchaseOrder_Id(purchaseOrderId);

        if (invoice == null) {
            throw new InvoiceNotFoundException("Invoice not found");
        }

        invoice.setPaid(true);
        invoice.setPaidDate(LocalDate.now());

        invoice = invoiceRepository.saveAndFlush(invoice);
        notificationService.remittenceNotification(invoice);
        return invoiceAssembler.toResource(invoice);
    }

    @Override
    public InvoiceDto getInvoice(Long purchaseOrderId) {
        Invoice invoice = invoiceRepository.findByPurchaseOrder_Id(purchaseOrderId);

        if (invoice == null) {
            throw new InvoiceNotFoundException("Invoice not found");
        }

        return invoiceAssembler.toResource(invoice);
    }

    @Override
    public List<InvoiceDto> getAllInvoices(String email) {
        return invoiceAssembler.toResources(invoiceRepository.findByPurchaseOrder_UserEmail(email));
    }
}
