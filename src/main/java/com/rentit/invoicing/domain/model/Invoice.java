package com.rentit.invoicing.domain.model;

import com.rentit.sales.domain.model.PurchaseOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Invoice {

    @Id
    @GeneratedValue
    Long id;

    boolean paid;

    LocalDate dueDate;

    LocalDate paidDate;

    BigDecimal amount;

    @OneToOne
    PurchaseOrder purchaseOrder;
}
