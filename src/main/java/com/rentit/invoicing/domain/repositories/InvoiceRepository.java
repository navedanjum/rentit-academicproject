package com.rentit.invoicing.domain.repositories;

import com.rentit.invoicing.domain.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    List<Invoice> findByPaid(boolean b);

    Invoice findByPurchaseOrder_Id(Long purchaseOrderId);

    List<Invoice> findByPurchaseOrder_UserEmail(String email);
}
