package com.rentit.common.application.controller.advice;

import com.rentit.common.application.dto.BaseResponse;
import com.rentit.common.application.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.net.URISyntaxException;

@RestControllerAdvice
public class GlobalControllerAdvice {

    @ExceptionHandler(PlantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public BaseResponse handPlantNotFoundException(PlantNotFoundException ex) {
        return new BaseResponse(ex.getMessage());
    }

    @ExceptionHandler(NoPlantAvailableException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public BaseResponse HandleNoPlantAvailableException(NoPlantAvailableException ex) {
        return new BaseResponse(ex.getMessage());
    }

    @ExceptionHandler(PurchaseOrderNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public BaseResponse HandlePruchaseOrderNotFoundException(PurchaseOrderNotFoundException ex) {
        return new BaseResponse(ex.getMessage());
    }

    @ExceptionHandler(InvoiceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public BaseResponse HandleInvoiceNotFoundException(InvoiceNotFoundException ex) {
        return new BaseResponse(ex.getMessage());
    }

    @ExceptionHandler(SecurityException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public BaseResponse HandleSecurityException(SecurityException ex) {
        return new BaseResponse(ex.getMessage());
    }

    @ExceptionHandler(StatusChangeException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public BaseResponse HandleStatusChangeException(StatusChangeException ex) {
        return new BaseResponse(ex.getMessage());
    }

    @ExceptionHandler(PlantInventoryItemNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public BaseResponse HandlePlantInventoryItemNotFoundException(PlantInventoryItemNotFoundException ex) {
        return new BaseResponse(ex.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseResponse HandleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        BindingResult bindingResult = ex.getBindingResult();
        BaseResponse baseResponse = new BaseResponse(bindingResult.getFieldError().getCode());
        return baseResponse;
    }

    @ExceptionHandler(URISyntaxException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public BaseResponse HandleURISyntaxException(URISyntaxException ex) {
        BaseResponse baseResponse = new BaseResponse("Something bad has happened");
        return baseResponse;
    }
}
