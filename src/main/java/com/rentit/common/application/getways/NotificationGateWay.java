package com.rentit.common.application.getways;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

import javax.mail.internet.MimeMessage;

@MessagingGateway
public interface NotificationGateWay {

    @Gateway(requestChannel="sendEmailChannel")
    void sendInvoice(MimeMessage msg);

    @Gateway(requestChannel="sendEmailChannel")
    void sendNotification(MimeMessage msg);
}
