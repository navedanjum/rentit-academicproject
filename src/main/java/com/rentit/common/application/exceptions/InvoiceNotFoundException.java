package com.rentit.common.application.exceptions;

public class InvoiceNotFoundException extends RuntimeException {
    public InvoiceNotFoundException(String msg) { super(msg); }
}
