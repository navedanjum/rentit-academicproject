package com.rentit.common.application.exceptions;

public class StatusChangeException extends RuntimeException {

    public StatusChangeException(String message) {
        super(message);
    }
}
