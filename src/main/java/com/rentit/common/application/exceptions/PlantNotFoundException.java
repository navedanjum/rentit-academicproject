package com.rentit.common.application.exceptions;

public class PlantNotFoundException extends RuntimeException{
    public PlantNotFoundException(Long id) {
        super(String.format("Plant not found! (Plant id: %d)", id));
    }
}
