package com.rentit.common.application.exceptions;

public class NoPlantAvailableException extends RuntimeException {

    public NoPlantAvailableException() {
        super("No Plant Available");
    }
}
