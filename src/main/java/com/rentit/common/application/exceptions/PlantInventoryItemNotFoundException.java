package com.rentit.common.application.exceptions;

public class PlantInventoryItemNotFoundException extends RuntimeException {

    public PlantInventoryItemNotFoundException(Long id) {
        super(String.format("Item not found! (Item id: %d)", id));
    }
}
