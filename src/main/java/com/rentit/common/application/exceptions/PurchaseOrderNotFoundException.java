package com.rentit.common.application.exceptions;

public class PurchaseOrderNotFoundException extends RuntimeException {

    public PurchaseOrderNotFoundException(Long id) {
        super(String.format("Purchase Order not found! (Purchase Order id: %d)", id));
    }
}
