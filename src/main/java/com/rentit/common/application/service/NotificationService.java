package com.rentit.common.application.service;

import com.rentit.invoicing.domain.model.Invoice;
import com.rentit.sales.domain.model.PurchaseOrder;

public interface NotificationService {

//    void updatePOStatusOfBuildIt(String status, Long purchaseOrderId);

    void invoiceCreation(Invoice invoice);

    void invoiceReminder(Invoice invoice);

    void notifyAboutExtension(boolean accepted, PurchaseOrder purchaseOrder, long extendedDays);

    void remittenceNotification(Invoice invoice);
}
