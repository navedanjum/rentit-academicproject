package com.rentit.common.application.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentit.common.application.getways.NotificationGateWay;
import com.rentit.invoicing.application.dto.InvoiceDto;
import com.rentit.invoicing.application.service.InvoiceAssembler;
import com.rentit.invoicing.domain.model.Invoice;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.application.services.PurchaseOrderAssembler;
import com.rentit.sales.domain.model.PurchaseOrder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;

@Slf4j
@Service
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    InvoiceAssembler invoiceAssembler;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    @Autowired
    NotificationGateWay notificationGateWay;

    @Value("${gmail.username}")
    String gmailUsername;

    @Value("${builtIt.secret}")
    String secret;

    private MultiValueMap<String, String> headers;

    @PostConstruct
    public void init() throws Exception {
        headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", secret);
    }


//    @Async
//    @Override
//    public void updatePOStatusOfBuildIt(String status, Long purchaseOrderId) {
//        try {
//            Thread.sleep(2000);
//            String url = "http://localhost:8082/api/hire-plant/update-PO/" + purchaseOrderId + "/" + status;
//            log.warn("updating status to buildIt, {} of purchase order: {}", status, purchaseOrderId);
//            HttpEntity<?> entity = new HttpEntity<>(headers);
//
//            ResponseEntity<Void> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, entity, Void.class);
//            log.debug("status: {}",responseEntity.getStatusCode());
//        } catch (Exception ex) {
//            log.error("<<<<<<<<<<<<<<Exception.>>>>>>>>>>>>>>>>>>, {}", ex);
//        }
//    }

    @Async
    @Override
    public void invoiceCreation(Invoice invoice) {

        JavaMailSender mailSender = new JavaMailSenderImpl();
        InvoiceDto invoiceDTO = invoiceAssembler.toResource(invoice);

        String invoiceJSON = null;
        try {
            invoiceJSON = mapper.writeValueAsString(invoiceDTO);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        MimeMessage rootMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;

        try {
            helper = new MimeMessageHelper(rootMessage, true);
            helper.setFrom(gmailUsername + "@gmail.com");
            helper.setTo(invoice.getPurchaseOrder().getUserEmail());
            helper.setSubject("Invoice for purchase order " + invoice.getPurchaseOrder().getId());
            helper.setText("Dear customer, \n\n Please find attached the invoice corresponding to your purchase order. \n\n Kindly yours, \n\n RentIt team!");
            helper.addAttachment("invoice.json", new ByteArrayDataSource(invoiceJSON, "application/json"));
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        notificationGateWay.sendInvoice(rootMessage);
    }

    @Async
    @Override
    public void invoiceReminder(Invoice invoice) {

        JavaMailSender mailSender = new JavaMailSenderImpl();
        InvoiceDto invoiceDTO = invoiceAssembler.toResource(invoice);

        String invoiceJSON = null;
        try {
            invoiceJSON = mapper.writeValueAsString(invoiceDTO);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        MimeMessage rootMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;

        try {
            helper = new MimeMessageHelper(rootMessage, true);
            helper.setFrom(gmailUsername + "@gmail.com");
            helper.setTo(invoice.getPurchaseOrder().getUserEmail());
            helper.setSubject("Invoice Reminder for purchase order " + invoice.getPurchaseOrder().getId());
            helper.setText("Dear customer, \n\n Please find attached the invoice corresponding to your purchase order. \n\n Kindly yours, \n\n RentIt team!");
            helper.addAttachment("invoice.json", new ByteArrayDataSource(invoiceJSON, "application/json"));
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        notificationGateWay.sendInvoice(rootMessage);
    }

    @Override
    public void notifyAboutExtension(boolean accepted, PurchaseOrder purchaseOrder, long extendedDays) {
        JavaMailSender mailSender = new JavaMailSenderImpl();
        PurchaseOrderDTO purchaseOrderDTO = purchaseOrderAssembler.toResource(purchaseOrder);

        String purchaserOrderJson = null;
        try {
            purchaserOrderJson = mapper.writeValueAsString(purchaseOrderDTO);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        MimeMessage rootMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;

        String subject;
        String body;

        if(accepted) {
            subject = "Purchase order extension accepted";
            body = "Your purchase order extension has been accepted";
        } else {
            subject = "Purchase order extension rejected";
            body = "Your purchase order extension has been rejected";
        }

        try {
            helper = new MimeMessageHelper(rootMessage, true);
            helper.setFrom(gmailUsername + "@gmail.com");
            helper.setTo(purchaseOrder.getUserEmail());
            helper.setSubject(subject);
            helper.setText("Dear customer, \n\n "+body+" \n\n Kindly yours, \n\n RentIt team!");
            helper.addAttachment("purchaserOrder.json", new ByteArrayDataSource(purchaserOrderJson, "application/json"));
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        notificationGateWay.sendInvoice(rootMessage);
    }

    @Override
    public void remittenceNotification(Invoice invoice) {
        JavaMailSender mailSender = new JavaMailSenderImpl();
        InvoiceDto invoiceDTO = invoiceAssembler.toResource(invoice);

        String invoiceJSON = null;
        try {
            invoiceJSON = mapper.writeValueAsString(invoiceDTO);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        MimeMessage rootMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;

        try {
            helper = new MimeMessageHelper(rootMessage, true);
            helper.setFrom(gmailUsername + "@gmail.com");
            helper.setTo(invoice.getPurchaseOrder().getUserEmail());
            helper.setSubject("Remittence for purchase order " + invoice.getPurchaseOrder().getId());
            helper.setText("Dear customer, \n\n Your remittence advise has been accepted. \n\n Kindly yours, \n\n RentIt team!");
            helper.addAttachment("invoice.json", new ByteArrayDataSource(invoiceJSON, "application/json"));
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        notificationGateWay.sendInvoice(rootMessage);
    }
}
